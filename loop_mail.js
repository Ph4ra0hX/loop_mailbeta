var express = require("express");
var bodyParser = require("body-parser");
var path = require("path");
var nodemailer = require("nodemailer");

var app = express();

app.use(express.static(path.join(__dirname, "public")));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/public/index.html"));
});

app.post("/enviar", function(req, res) {
  res.sendFile(path.join(__dirname + "/public/enviar.html"));

  let emailLogin = req.body.emailLogin;
  let senhaLogin = req.body.senhaLogin;

  transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: emailLogin,
      pass: senhaLogin
    }
  });
});

app.post("/enviaremail", function(req, res) {
  res.sendFile(path.join(__dirname + "/public/enviado.html"));
  let emails;
  let separado;
  delete separado;
  delete emails;
  emails = req.body.destinatarios;
  separado = emails.split(",");

  for (i = 0; i < separado.length; i++) {
    console.log(separado[i]);
    var mailOptions = {
      from: req.body.emailLogin,
      to: separado[i],
      subject: req.body.titulo,
      text: req.body.texto
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
        res.send(200);
      }
    });
  }
});

app.listen(3005, function() {
  console.log("Rodando!!!");
});
